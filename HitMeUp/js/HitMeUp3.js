// An example Parse.js Backbone application based on the todo app by
// [Jérôme Gravel-Niquet](http://jgn.me/). This demo uses Parse to persist
// the todo items and provide user authentication and sessions.

$(function() {

  Parse.$ = jQuery;

  // Initialize Parse with your Parse application javascript keys
Parse.initialize("uMFvrd4Uuek0sHSdvtWND5PoH0fvx8iw4aMfGTqd", "szwrOozqJAia97T4pXCxdpqiV5RCQoShKNH6z0ku");


  function uniqueIdentifier() {
    var result = '';
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for (var i = 7; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
  };

  // Todo Model
  // ----------
  var mediaLink = Parse.Object.extend("MediaLink", {
    defaults: {
      service: "blank",
      url: "about:blank", 
      username: "blank"
    },
  });
  // Our basic Todo model has `content`, `order`, and `done` attributes.
  /*var Todo = Parse.Object.extend("Todo", {
    // Default attributes for the todo.
    defaults: {
      content: "empty todo...",
      done: false
    },

    // Ensure that each todo created has `content`.
    initialize: function() {
      if (!this.get("content")) {
        this.set({"content": this.defaults.content});
      }
    },

    // Toggle the `done` state of this todo item.
    toggle: function() {
      this.save({done: !this.get("done")});
    }
  });*/

  // This is the transient application state, not persisted on Parse
  var AppState = Parse.Object.extend("AppState", {
    defaults: {
      filter: "all"
    }
  });

  // -------ManageMedia--------

  // The main view that lets a user manage their todo items
 var ManageMediaView = Parse.View.extend({
    events: {
      "submit form.twitter-form": "addTwitter",
      "click .log-out": "logOut"
    },

    el: ".content",

    initialize: function() {
      
      _.bindAll(this, "logOut", "addTwitter");
      this.render()
    },
      
      
    logOut: function(e) {
      Parse.User.logOut();
      new LogInView();
      this.undelegateEvents();
      delete this;
    },

    addTwitter: function(e) {
      var currentUser = Parse.User.current();
      var self = this;
      var service = "Twitter";
      var username = this.$("#twitter_name").val();
      var url = "twitter.com/" + username;
      alert(url);
      currentUser.set("twitter", url);
      currentUser.save(null);
      /*this.set("twitter", url);
      alert(this.get("twitter"))
      var Twitter = new MediaLink(); 
      Twitter.set("url", url);
      Twitter.set("service", service);
      Twitter.set("username", username);
      currentUser.set("twitter", url);
      alert(url);*/

    },

    render: function() {
      this.$el.html(_.template($("#manage-media-template").html()));
      this.delegateEvents();
    },
});

  var LogInView = Parse.View.extend({
    events: {
      "submit form.login-form": "logIn",
      "submit form.signup-form": "signUp",
      "submit form.code-form" : "codeSearch", 
    },

    el: ".content",
    
    initialize: function() {
      _.bindAll(this, "logIn", "signUp");
      this.render();
    },

    logIn: function(e) {
      var self = this;
      var username = this.$("#login-username").val();
      var password = this.$("#login-password").val();
      
      Parse.User.logIn(username, password, {
        success: function(user) {
          new ManageMediaView();
          self.undelegateEvents();
          delete self;
        },

        error: function(user, error) {
          self.$(".login-form .error").html("Invalid username or password. Please try again.").show();
          self.$(".login-form button").removeAttr("disabled");
        }
      });

      this.$(".login-form button").attr("disabled", "disabled");

      return false;
    },

    signUp: function(e) {
      var self = this;
      var username = this.$("#signup-username").val();
      var password = this.$("#signup-password").val();
      var id = uniqueIdentifier();
      
      Parse.User.signUp(username, password, { ID: id }, {
        success: function(user) {
          new ManageMediaView();
          self.undelegateEvents();
          delete self;
        },

        error: function(user, error) {
          self.$(".signup-form .error").html(_.escape(error.message)).show();
          self.$(".signup-form button").removeAttr("disabled");
        }

      });

      this.$(".signup-form button").attr("disabled", "disabled");

      return false;
  },

    codeSearch: function(e) {
        var self = this;
        var code = this.$("#user-code").val();
        var query = new Parse.Query(Parse.User);

        query.equalTo("ID", code);
        query.find({
          success: function(user) {
            alert(user[0].getUsername());
          },

          error: function(user, error) {
            self.$(".code-form .error").html("user not found");
            self.$(".code-form button").removeAttr("disabled");
          }

        });

        this.$(".code-form button").attr("disabled", "disabled");

        return false;
    },


    render: function() {
      this.$el.html(_.template($("#login-template").html()));
      this.delegateEvents();
    }
  });

  // The main view for the app
  var AppView = Parse.View.extend({
    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: $("#todoapp"),

    initialize: function() {
      this.render();
    },

    render: function() {
      if (Parse.User.current()) {
        new ManageMediaView();
      } else {
        new LogInView();
      }
    }
  });

  var AppRouter = Parse.Router.extend({
    routes: {
      "all": "all",
      "active": "active",
      "completed": "completed"
    },

    initialize: function(options) {
    },

    all: function() {
      state.set({ filter: "all" });
    },

    active: function() {
      state.set({ filter: "active" });
    },

    completed: function() {
      state.set({ filter: "completed" });
    }
  });

  var state = new AppState;

  new AppRouter;
  new AppView;
  Parse.history.start();
});
